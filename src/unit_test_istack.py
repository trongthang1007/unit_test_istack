from IStack import IStack, StackEmptyException
import pytest
import random

class TestIStack():
   
    items = [random.random for _ in range(10)]
    istack = IStack()
        
    def test_istack_pop_exception(self):
        with pytest.raises(StackEmptyException):
            self.istack.pop()   
    
    def test_istack_peek_is_empty(self):
        with pytest.raises(StackEmptyException):
            self.istack.peek()

    def test_istack_push(self):
        self.istack.push(self.items[0])
        assert self.istack.peek() == self.items[0]

    def test_istack_pop_success(self):
        peek = self.istack.peek()
        val = self.istack.pop()
        assert peek == val

    def test_istack_contains(self):
        for i in self.items:
            self.istack.push(i) 
        assert self.istack.contains(self.items[0])

    def test_istack_peek_is_not_empty(self):
        assert self.istack.peek() == self.items[len(self.items) - 1]
    
    def test_istack_clear(self):
        self.istack.clear()
        assert self.istack.is_empty()

    def test_istack_is_empty(self):
        assert self.istack.is_empty()

    