class IStack:
    def __init__(self):
        self.items = []

    def clear(self):
        self.items.clear()

    def contains(self, value):
        return value in self.items

    def peek(self):
        if not self.is_empty():
            return self.items[-1]
        else:
            raise StackEmptyException("Stack is empty")

    def push(self, value):
        self.items.append(value)

    def pop(self):
        if not self.is_empty():
            return self.items.pop()
        else:
            raise StackEmptyException("Stack is empty")

    def is_empty(self):
        return self.items == []

class StackEmptyException(Exception):
	pass
